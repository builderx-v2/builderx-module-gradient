declare type coords = {
    x: number;
    y: number;
};
declare type stop = {
    stopColor: string;
    offset: number;
};
declare type gradientJSONType = {
    gradientType: string;
    from: coords;
    to: coords;
    stops: stop[];
    shouldSmoothenOpacity: boolean;
    activeIndex: number;
};
export default function getGradientValue(gradientJSON: gradientJSONType): string;
export {};
