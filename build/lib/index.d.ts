export { default as getGradientValue } from "./GradientFinder";
export { default as getGradientImage } from "./GradientImage";
