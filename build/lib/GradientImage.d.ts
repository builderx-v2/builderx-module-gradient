declare type coords = {
    x: number;
    y: number;
};
declare type stop = {
    stopColor: string;
    offset: number;
};
declare type gradientJSONType = {
    gradientType: string;
    from: coords;
    to: coords;
    stops: stop[];
    shouldSmoothenOpacity: boolean;
    activeIndex: number;
};
declare type dimensionType = {
    height: number | string;
    width: number | string;
};
export default function getGradientImage(gradientJSON: gradientJSONType, elementDims: dimensionType): Promise<string>;
export {};
