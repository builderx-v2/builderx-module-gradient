import * as React from "react";

import { getGradientValue } from "../lib";
import { getGradientImage } from "../lib";

const gradientJSON = {
  gradientType: "LinearGradient",
  from: { x: 0.5, y: 0.1 },
  to: { x: 0.5, y: 0.9 },
  stops: [
    { stopColor: "#E70B0B", offset: 0 },
    { stopColor: "#EEEEEE", offset: 1 },
  ],
  shouldSmoothenOpacity: false,
  activeIndex: 1,
};

const dim = {
  height: 400,
  width: 600,
};

class Gradient extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      base64: null,
    };
    this.handleBase = this.handleBase.bind(this);
  }

  handleBase(newbase: string) {
    this.setState({
      base64: newbase,
    });
  }
  componentDidMount() {
    getGradientImage(gradientJSON, dim).then((base64: string) => {
      this.handleBase(base64);
    });
  }

  render() {
    return <img src={this.state.base64} />;
  }
}

class App extends React.Component<any, any> {
  componentDidMount() {
    getGradientValue(gradientJSON);
  }

  public render() {
    return (
      <div>
        <div>String of gradient is {}</div>
        <br />
        <p>Base 64 of Image Gradient Image is:</p>
        <Gradient />
      </div>
    );
  }
}

export default App;
