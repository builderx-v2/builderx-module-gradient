import domtoimage from "dom-to-image";
import { getGradientValue } from "./";

type coords = {
  x: number;
  y: number;
};

type stop = {
  stopColor: string;
  offset: number;
};

type gradientJSONType = {
  gradientType: string;
  from: coords;
  to: coords;
  stops: stop[];
  shouldSmoothenOpacity: boolean;
  activeIndex: number;
};

type dimensionType = {
  height: number | string;
  width: number | string;
};

export default function getGradientImage(
  gradientJSON: gradientJSONType,
  elementDims: dimensionType
): Promise<string> {
  // use dom-to-image package for converting div to image
  return new Promise((resolve, reject) => {
    const gradientString = getGradientValue(gradientJSON);
    const newDiv = document.createElement("div");
    newDiv.setAttribute("style", "");
    newDiv.style.backgroundImage = gradientString;
    newDiv.style.width = elementDims.width ? elementDims.width + "px" : "100px";
    newDiv.style.height = elementDims.height
      ? elementDims.height + "px"
      : "100px";

    newDiv.style.zIndex = "-999";
    newDiv.style.position = "absolute";

    let wrapperDiv = document.getElementById("wrapperDiv");
    if (!wrapperDiv) {
      wrapperDiv = document.createElement("div");
      wrapperDiv.setAttribute("style", "");
      wrapperDiv.setAttribute("id", "wrapperDiv");
      wrapperDiv.style.overflow = "hidden";
      document.body.prepend(wrapperDiv);
    }
    wrapperDiv.prepend(newDiv);
    domtoimage
      .toPng(newDiv)
      .then((dataUrl: string) => {
        if (wrapperDiv) {
          wrapperDiv.removeChild(newDiv);
          if (wrapperDiv.childElementCount === 0) {
            document.body.removeChild(wrapperDiv);
          }
        }
        resolve(dataUrl);
      })
      .catch((error: any) => {
        console.error("oops, something went wrong!", error);
      });
  });
}
