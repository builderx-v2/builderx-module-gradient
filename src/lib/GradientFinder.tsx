type coords = {
  x: number;
  y: number;
};

type stop = {
  stopColor: string;
  offset: number;
};

type gradientJSONType = {
  gradientType: string;
  from: coords;
  to: coords;
  stops: stop[];
  shouldSmoothenOpacity: boolean;
  activeIndex: number;
};

export default function getGradientValue(gradientJSON: gradientJSONType) {
  let colors: string[] = [];

  gradientJSON.stops.forEach((data: stop, index: number) => {
    colors.push(data.stopColor);
  });
  let vector;
  if (gradientJSON.to && gradientJSON.from) {
    vector = {
      x: (gradientJSON.from.y as number) - (gradientJSON.to.y as number),
      y: (gradientJSON.from.x as number) - (gradientJSON.to.x as number),
    };
  } else {
    vector = {
      x: 0,
      y: 2,
    };
  }
  const angleRad = Math.atan2(vector.y, vector.x);
  const angleDeg = (angleRad * 180) / Math.PI;

  let realLocations: number[] = [];

  gradientJSON.stops.forEach((data: any, index: number) => {
    realLocations.push(data.offset);
  });

  const colorStrings: string = colors
    .map(
      (color: any, i: number) =>
        `${color} ${Math.round(realLocations[i] * 100)}%`
    )
    .join(", ");

  if (
    gradientJSON.gradientType &&
    gradientJSON.gradientType === "LinearGradient"
  ) {
    return `linear-gradient(${angleDeg}deg, ${colorStrings})`;
  } else if (
    gradientJSON.gradientType &&
    gradientJSON.gradientType === "RadialGradient"
  ) {
    return `radial-gradient(${colorStrings})`;
  } else if (
    gradientJSON.gradientType &&
    gradientJSON.gradientType === "AngularGradient"
  ) {
    return `conic-gradient(${colorStrings})`;
  } else {
    return `linear-gradient(${angleDeg}deg, ${colorStrings})`;
  }
}
